﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Eva3.Models;

namespace Eva3.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Eva3.Models.Calzado> Calzado { get; set; }
        public DbSet<Eva3.Models.Material> Material { get; set; }
        public DbSet<Eva3.Models.Reparacion> Reparacion { get; set; }
        public DbSet<Eva3.Models.Formulario> Formulario { get; set; }
    }
}
