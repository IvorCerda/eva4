﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eva3.Data.Migrations
{
    public partial class MigracionCalzado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Calzado",
                columns: table => new
                {
                    CalzadoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NombreProducto = table.Column<string>(nullable: true),
                    TipoCalzado = table.Column<string>(nullable: true),
                    PrecioProducto = table.Column<int>(nullable: false),
                    StockCalzado = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Calzado", x => x.CalzadoId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Calzado");
        }
    }
}
