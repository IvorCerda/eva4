﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Eva3.Data.Migrations
{
    public partial class MigracionReparacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Reparacion",
                columns: table => new
                {
                    ReparacionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true),
                    Telefono = table.Column<int>(nullable: false),
                    Correo = table.Column<string>(nullable: true),
                    Precio = table.Column<int>(nullable: false),
                    FechaIngreso = table.Column<string>(nullable: true),
                    EstadoCalzado = table.Column<string>(nullable: true),
                    FechaEntrega = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reparacion", x => x.ReparacionId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reparacion");
        }
    }
}
