﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eva3.Models
{
    public class Material
    {
        public int MaterialId { get; set; }
        public string MaterialTipo { get; set; }
        public int MaterialCosto {get; set;}
        public int StockMaterial { get; set; }
    }
}
