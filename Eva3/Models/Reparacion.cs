﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eva3.Models
{
    public class Reparacion
    {
        //id-nombre-telefono-correo-estadoDelCalzado-Precio-FechaIngreso
        public int ReparacionId { get; set; }
        public string Nombre { get; set; }
        public int Telefono { get; set; }
        public string Correo { get; set; }
        public int Precio { get; set; }
        public string FechaIngreso { get; set; }
        public string EstadoCalzado { get; set; }
        public string FechaEntrega { get; set; }





        
    }
}
