﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eva3.Models
{
    public class Formulario
    {
       
        public int FormularioId { get; set; }
        public string NombreUsuario { get; set; }
        public string Email { get; set; }
        public string Asunto { get; set; }
        public string Mensaje { get; set; }
            
        
    }
}
