﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Eva3.Data;
using Eva3.Models;
using Microsoft.AspNetCore.Authorization;

namespace Eva3.Controllers
{
    public class CalzadosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CalzadosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Calzados
        public async Task<IActionResult> Index()
        {
            return View(await _context.Calzado.ToListAsync());
        }

        // GET: Calzados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calzado = await _context.Calzado
                .FirstOrDefaultAsync(m => m.CalzadoId == id);
            if (calzado == null)
            {
                return NotFound();
            }

            return View(calzado);
        }
        //page "GRACIAS POR SU COMPRA"
        public async Task<IActionResult> Comprado(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calzado = await _context.Calzado
                .FirstOrDefaultAsync(m => m.CalzadoId == id);
            if (calzado == null)
            {
                return NotFound();
            }

            return View(calzado);
        }
        [Authorize]
        // GET: Calzados/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Calzados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CalzadoId,NombreProducto,TipoCalzado,PrecioProducto,StockCalzado")] Calzado calzado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(calzado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(calzado);
        }

        // GET: Calzados/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calzado = await _context.Calzado.FindAsync(id);
            if (calzado == null)
            {
                return NotFound();
            }
            return View(calzado);
        }

        // POST: Calzados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CalzadoId,NombreProducto,TipoCalzado,PrecioProducto,StockCalzado")] Calzado calzado)
        {
            if (id != calzado.CalzadoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(calzado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CalzadoExists(calzado.CalzadoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(calzado);
        }

        // GET: Calzados/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calzado = await _context.Calzado
                .FirstOrDefaultAsync(m => m.CalzadoId == id);
            if (calzado == null)
            {
                return NotFound();
            }

            return View(calzado);
        }

        // POST: Calzados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var calzado = await _context.Calzado.FindAsync(id);
            _context.Calzado.Remove(calzado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CalzadoExists(int id)
        {
            return _context.Calzado.Any(e => e.CalzadoId == id);
        }
    }
    
}
